package com.msopentech.authDialog;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AuthRequestHandler extends CordovaPlugin {

    private static String login="";
    private static String password="";
@Override
public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
    if (action.equals("authenticate")) {
        //initial attempt-> passing credentials to plugin
        //needs to be generalized
         login = args.getString(1);
         password = args.getString(2);
        return true;
    }
    return false;
}




    public boolean onReceivedHttpAuthRequest(CordovaWebView view, final ICordovaHttpAuthHandler handler, String host, String realm) {
        AuthenticationDialog dialog = new AuthenticationDialog(cordova.getActivity(), host, realm);

        dialog.setOkListener(new AuthenticationDialog.OkListener() {
            public void onOk(String host, String realm, String username, String password) {
                handler.proceed(username, password);
            }
        });

        dialog.setCancelListener(new AuthenticationDialog.CancelListener() {
            public void onCancel() {
                handler.cancel();
            }
        });

        // dialog.show();

        handler.proceed(login,password);


        return true;
    }
}